package main

import (
	"github.com/murlokswarm/app"
	"github.com/murlokswarm/app/drivers/mac"
)

func init() {
	app.Import(&Home{})
}

type Home app.ZeroCompo

// Render returns return the HTML describing the home screen.
func (h *Home) Render() string {
	return `
<div class="Home">
	<div class="Example">
		<h1>Copy/paste</h1>
		<ul oncontextmenu="OnContextMenu">
			<li>Select me</li>
			<li>Right click</li>
			<li>Copy</li>
		</ul>
		<textarea placeholder="Right click/Paste or use meta + v" oncontextmenu="OnContextMenu"></textarea>
	</div>
	<div class="Example">
		<h1>Custom menu</h1>
		<button onclick="OnButtonClick">Show</button>
	</div>
</div>
	`
}

func main() {
	app.Run(&mac.Driver{
		URL: "/Home",
		MenubarConfig: mac.MenuBarConfig{
			// Overrides the default edit menu.
			EditURL: "/EditMenu",

			// Adds the custom menu in the menubar.
			CustomURLs: []string{"/CustomMenu"},
		},
	})
}
