package main
import (
	"fmt"
	"math"
	"runtime"
	"time"
)

/*
 *  流程控制的用法
 */
func main() {

	//if只支持一个初始化语句，初始化语句与判断语句以;分割。 初始化语句也可以拿出来单独赋值。

	if s := "马"; s == "马云" {
		fmt.Printf("阿里巴巴\n")
	}else if s == "马化腾" {
		fmt.Printf("腾讯\n")
	}else{
		fmt.Printf("xxx\n")
	}
	fmt.Printf("--------------------1----------------------\n")

	//操作系统
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
	fmt.Printf("--------------------2----------------------\n")

	//
	fmt.Println("什么时候周六?")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("Today.")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In two days.")
	default:
		fmt.Println("Too far away.")
	}
	fmt.Printf("--------------------3----------------------\n")

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
	fmt.Printf("--------------------4----------------------\n")

	fmt.Println(
		pow(3, 2, 10),
		pow(3, 3, 20),
	)

}

func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	}
	return lim
}
