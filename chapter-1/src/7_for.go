package main
import "fmt"

/*
 *  循环的用法
 */
func main() {
	sum := 1
	for sum < 4 {
		sum += sum
	}
	fmt.Println(sum)


	//不写条件 就是 死循环   虽然报红，但是不妨碍运行。
	for {
	}
}