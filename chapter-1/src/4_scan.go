package main
import (
	"fmt"
	//"errors"   //如果导入的包不用，会报错 ：imported and not used: "errors"。
)

/*
 *  用户输入的用法
 */
func main() {
	var a int

	fmt.Printf("请输入变量a:")

	//阻塞等待用户的输入
	//fmt.Scanf("%d",&a)  //别忘了&

	//或 使用Scan：
	fmt.Scan(&a)

	fmt.Println("a=",a)
}
