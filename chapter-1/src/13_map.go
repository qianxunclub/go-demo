package main

import "fmt"

/*
 * Map的用法
 */
 //结构
type Ver struct {
	Lat, Long float64    //变量名,变量名(Long这个变量名起得太坑爹了，我还以为是类型别名呢，我了个艹艹艹的！)  类型  。 我猜的。
}

//map[键的类型]值的类型
var m map[string]Ver

var ma = map[string]Ver{
	"Bell Labs": Ver{40.68433, -74.39967,},
	"Google": Ver{37.42202, -122.08408,},
}
//如果顶级类型只是一个类型名，可以省略它。如下：省略了Ver。  这个全局变量没有使用，竟然没报错。
var mapa = map[string]Ver{
	"Bell Labs": {40.68433, -74.39967},
	"Google":    {37.42202, -122.08408},
}

func main() {
	fmt.Println(ma)
	fmt.Printf("--------------------1----------------------\n")
	/*
	 *  就是键值对儿，map的零值为nil。nil的map没有键，也不能添加键。make函数会 返回 指定类型的map，初始化并准备好使用。 make(map[键的类型]值的类型)
	 */
	m = make(map[string]Ver)

	//设置键值对   m[键]= 值 结构{}
	m["Bell Labs"] = Ver{
		40.68433, -74.39967,
	}

	//通过键获取值
	fmt.Println(m["Bell Labs"])
	fmt.Printf("--------------------2----------------------\n")

	m := make(map[string]int)//m 重新初始化    增删改查：

	m["Answer"] = 42
	fmt.Println("新增后 The value:", m["Answer"])

	m["Answer"] = 48
	fmt.Println("更新后 The value:", m["Answer"])
	v, ok1 := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok1)	//原来这ok1是 键存在就是true，键不存在就是false。 我猜的。

	delete(m, "Answer")
	fmt.Println("删除后 The value:", m["Answer"])

	v, ok := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok)

}

