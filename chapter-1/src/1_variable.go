//一个工程(或文件夹下)必须有且只能有一个main包和main方法，但是每个文件里都写了main，不知道为什么没报错？？？？？？？？？
package main

import (
		"fmt"
	"strconv"
)

/*
 *  变量的用法
 */
func main() {
	//var a int					//不赋值，默认为类型的默认值，int为0
	//var b = 5
	////c:=6						 //如果声明的变量不用，会报错：c declared and not used。
	//var (
	//	d=2
	//	e=12345.5
	//)
	//i, j := 10, 20                //方法内声明变量且赋值，
	//i, j = j, i                   //交换两个变量的值
	//fmt.Printf("  i=%d,\n  j=%d,\n\n  a=%d,\n  b=%d,\n  d=%d,\n  e=%8.1f,\n\n", i, j,a,b,d,e) //格式化输出
	//fmt.Printf("  e type is %T\n", e)

	//委员会人数
	commiteeCount := Decimal(4)
	//每块奖励币数
	total := Decimal(5)
	//平均每个委员会奖励币数
	average := Decimal(5)

	fmt.Printf("\n分之前，total is %.2f\n",total)
	average = Decimal(average/commiteeCount)
	fmt.Printf("\n分之后，average is %.2f\n",average)
	fmt.Printf("\n%.2f-%.2f*(%.2f-1)\n",total,average,commiteeCount)
	average=Decimal(total-average*(commiteeCount-1))
	fmt.Printf("\nleader is %.2f\n",average)
	fmt.Printf("\n15个人分13个币的话，14个人是0.87，leader是0.82 \n")

}
func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}

