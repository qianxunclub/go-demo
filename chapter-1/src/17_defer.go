package main

import (
	"fmt"
)

/*
 * defer延迟执行与匿名函数的结合使用
 */
func main() {
	a,b:=1,2
	defer func() {	//先执行外部，此时值已被外部改变
		fmt.Printf("\n （一）、匿名函数内部：a=%d,b=%d\n",a,b)
	}() //代表调用此匿名函数
	a=3
	b=4
	fmt.Printf("\n （二）、第一个匿名函数外部：a=%d,b=%d\n",a,b)


	c,d:=5,6
	defer func(c,d int) {
		fmt.Printf("\n （三）、第一个匿名函数内部：c=%d,d=%d\n",c,d)
	}(c,d) //代表调用此匿名函数，把参数传递过去，已经先传递参数，只是没有调用。
	c=7
	d=8
	fmt.Printf("\n （四）、第一个匿名函数外部：c=%d,d=%d\n",c,d)
}

