package main

import (
	"fmt"
	"strings"
)

/*
 *  切片的用法
 */
func main() {
	/*
	 * 切片不存储任何数据，它只描述底层数组的一部分。 更改切片的元素会修改其基础数组的相应元素。 共享相同底层数组的其他切片将看到这些更改。
	 * 切片就像是对数组的引用。
	 */
	names := [4]string{
		"0_John",
		"1_Paul",
		"2_George",
		"3_Ringo",
	}
	fmt.Println(names)
	fmt.Printf("--------------------1----------------------\n")

	/*
	 * 前包 后不包。 切片时，可省略使用其默认值的上限或下限。 下限的默认值为零，上限的默认值为切片的长度。即 names[0:4]，names[:4]，names[0:]，names[:]等价。
	 */
	s1 := names[0:2]		//切片s1包含names下标为0、1的元素
	s2 := names[1:3]		//切片s2包含names下标为1、2的元素
	fmt.Println(s1, s2)
	fmt.Printf("--------------------2----------------------\n")

	s2[0] = "XXX"		//切片的第0个元素 即 1_Paul 重新赋值为XXX
	fmt.Println(s1, s2)	//其他切片 如s1受到影响
	fmt.Println(names)	//原数组也受到影响
	fmt.Printf("--------------------3----------------------\n")


	/*
	 * 切片具有长度和容量。 切片的长度是切片包含的元素数。 切片的容量是基础数组中元素的数量，从切片中的第一个元素开始计算。
	 * 可以使用表达式len（s）和cap（s）来获得切片s的长度和容量。 只要具有足够的容量，可以通过重新切片来延长切片的长度。
	 * 尝试更改示例程序中的一个切片操作，将其扩展到超出其容量范围，看看会发生什么。
	 */
	s3 := []int{2, 3, 5, 7, 11, 13}	//基础数组 6个元素
	printSlice(s3)

	// Slice the slice to give it zero length.  切个0长度的切片
	s3 = s3[:0]	//这里是重新复制，为什么基础数组没被覆盖为0呢，好奇怪。？？？？？？？？？？？？？？？？？？？？？
	printSlice(s3)

	// Extend its length.
	s3 = s3[:4]		//从开头儿 切到下标为3的元素
	printSlice(s3)

	// Drop its first two values.	//丢弃前2个元素
	s3 = s3[2:]
	printSlice(s3)
	fmt.Printf("--------------------4----------------------\n")

	//切片的零值为nil。nil的切片的长度和容量为0，并且没有基础数组。
	var s4 []int
	fmt.Println(s4, len(s4), cap(s4))
	if s4 == nil {
		fmt.Println("nil!")
	}
	fmt.Printf("--------------------5----------------------\n")

	/*
	 * 可以用内置的make函数创建切片;这就是创建动态大小数组的方式。make函数分配一个zeroed数组并返回一个指向该数组的切片。要指定容量，请传递第三个参数。
	 */
	s5 := make([]int, 5)
	printSliceStr("s5", s5)

	s6 := make([]int, 0, 5)
	printSliceStr("s6", s6)

	s7 := s6[:2]
	printSliceStr("s7", s7)

	s8 := s7[2:5]
	printSliceStr("s8", s8)
	fmt.Printf("--------------------6----------------------\n")

	/*
	 * 将新元素附加到一个切片上是很常见的，因此Go提供了一个内置的append函数。func append（s T，vs.T）T的第一个参数s是一个类型T的切片，其余的是T值附加到片上。
	 * append的结果是一个包含原始切片的所有元素和新提供的值的切片。如果s的后备数组太小，不足以容纳所有给定的值，那么就会分配更大的数组。返回的切片将指向新分配的数组。
	 */
	s8 = append(s8, 2, 3, 4)
	printSlice(s8)

	fmt.Printf("--------------------7----------------------\n")
	/*
	 * 切片可以包含任何类型，包括其他切片。
	 */
	// Create a tic-tac-toe board. 创建一个井字板
	//如果我没猜错的话，这貌似是二维切片
	board := [][]string{
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
	}

	// The players take turns. 玩家轮流。
	board[0][0] = "X"
	board[2][2] = "O"
	board[1][2] = "X"
	board[1][0] = "O"
	board[0][2] = "X"

	for i := 0; i < len(board); i++ {
		fmt.Printf("%s\n", strings.Join(board[i], " "))
	}
	fmt.Printf("--------------------8----------------------\n")

	/*
	 * 一个切片就像是没有长度的数组。这是一个数组：[3]bool{true, true, false}。
	 * []bool{true, true, false}，这句创建了一个相同的数组，然后创建了一个切片指向这个数组。
	 * 我也不知道这个示例是在搞毛。
	 */
	q := []int{2, 3, 5, 7, 11, 13}
	fmt.Println(q)

	r := []bool{true, false, true, true, false, true}
	fmt.Println(r)

	s := []struct {
		i int
		b bool
	}{
		{2, true},
		{3, false},
		{5, true},
		{7, true},
		{11, false},
		{13, true},
	}
	fmt.Println(s)
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func printSliceStr(s string, x []int) {
	fmt.Printf("%s len=%d cap=%d %v\n",
		s, len(x), cap(x), x)
}
