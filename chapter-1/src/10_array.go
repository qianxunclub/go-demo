package main
import "fmt"

/*
 *  数组的用法
 */
func main() {
	var a [2]string		//数组长度不可变
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(a[0], a[1])
	fmt.Println(a)

	primes := [6]int{2, 3, 5, 7, 11, 13}		//数组 声明并赋值
	fmt.Println(primes)

}



