package main
import "fmt"

/*
 *  指针的用法  *变量=变量的值  &变量=变量的内存地址
 */
func main() {

	i, j := 42, 2701

	p := &i         // point to i  					指向i

	fmt.Println(*p) // read i through the pointer	通过指针读取i
	fmt.Println(&p) // p的内存地址
	fmt.Println(&i) // i的内存地址

	*p = 21         // set i through the pointer	通过指针设置i
	fmt.Println(i)  // see the new value of i		看下i的新值

	p = &j         // point to j					指向j
	*p = *p / 37   // divide j through the pointer	通过指针进行除法运算
	fmt.Println(j) // see the new value of j		看下j的新值

}