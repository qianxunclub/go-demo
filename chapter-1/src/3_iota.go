package main
import (
	"fmt"
	//"errors"   //如果导入的包不用，会报错 ：imported and not used: "errors"。
)

/*
 *  iota枚举的用法
 */
func main() {
	//1、iota常量 自动生成器，每隔一行，自动累加1
	//2、iota给常量赋值使用
	const (
		a = iota
		b = iota
		c = iota
	)
	//3、iota遇到const，重置为0
	const d = iota
	//4、可以只写一个iota
	const (
		e = iota
		f
		g
	)
	fmt.Printf("  a=%d,  b=%d,  c=%d,  d=%d,  e=%d,  f=%d,  g=%d,\n", a,b,c,d,e,f,g)

	//5,如果是在同一行，值都一样
	const (
		h = iota
		i,j,k = iota,iota,iota
		l=iota
	)
	fmt.Printf("  h=%d,  i=%d,  j=%d,  k=%d,  l=%d,\n", h,i,j,k,l)

}
