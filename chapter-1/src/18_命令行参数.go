package main

import (
	"fmt"
	"os"
)

/*
 * 命令行参数
 */
func main() {
	list := os.Args

	for i,data:=range list{
		fmt.Printf("list[%d]=%s\n",i,data)
	}
/*  命令行运行结果
 *	$ go run 18_命令行参数.go a b
 *
 *  list[0]=/var/folders/m1/6nhxg2rn7wb1yq27nnqnks0w0000gp/T/go-build632785804/b001/exe/18_命令行参数
 *  list[1]=a
 *  list[2]=b
 */

}

