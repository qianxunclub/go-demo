package main

import "fmt"

/*
 * 闭包的用法
 */
 //函数的返回值是一个匿名函数，返回一个函数类型 func(int) int
func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {

	/*
	 * 返回值为一个匿名函数，返回一个函数类型，通过f来调用返回的匿名函数，f来调用闭包函数。
	 * 它不关心这些捕获了的变量和常量是否已超出作用域，只要闭包还在使用它，这些变量就还会存在。
	 */
	f:=adder()
	fmt.Println(f(1))	//结果1
	fmt.Println(f(2))	//结果3
	fmt.Println(f(3))	//结果6

}

