package main

import (
	"math"
	"fmt"
)

/*
 *函数也是值。它们可以像其他值一样传递。函数值可以用作函数参数和返回值。
 */

 //官方没具体说明意思，以下全是我的猜想，不知对不对。
 //compute 函数 的参数 是一个函数，给这个函数 取了个变量名fn，fn函数的类型为 float64，compute函数的返回值 也是float64。
func compute(fn func(float64, float64) float64) float64 {
	return fn(3, 4)
}

func main() {
	//这里是定义了一个函数hypot，两个参数 和 一个返回值 全为float64类型。 x*x+y*y，再开方
	hypot := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}
	fmt.Println(hypot(5, 12))  // 5*5+12*12，开方  结果为13

	fmt.Println(compute(hypot))		//把hypot这个函数 作为参数 传给函数compute 。3*3+4*4=25，开方，结果为5。
	fmt.Println(compute(math.Pow))	//内置函数 Pow(x, y float64) 计算x的y次方，3的4次方 结果为81。
}
