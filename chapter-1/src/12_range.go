package main

import (
	"fmt"
)

/*
 *  range的用法
 */
var s = []int{1, 2, 4, 8}

func main() {
	//range遍历一个切片或map。当对一个切片进行取值时，每个迭代都会返回两个值。第一个是下标，第二个是该下标中的元素的副本。
	for i, v := range s {
		fmt.Printf("2**%d = %d\n", i, v)
	}
	fmt.Printf("--------------------1----------------------\n")

	pow := make([]int, 5)	//创建一个切片
	for i := range pow {
		pow[i] = 1 << uint(i) // == 2**i		//为每个元素赋值 2的i次方
	}

	for _, value := range pow {
		fmt.Printf("%d\n", value)		//遍历，下标不需要，用下划线丢弃下标。
	}
}

