package main
import (
	"fmt"
	//"errors"   //如果导入的包不用，会报错 ：imported and not used: "errors"。
)

/*
 *  类型别名的用法
 */
func main() {
	//给类型 起个别名
	type bigint int64
	var a bigint
	fmt.Printf("  a type is %T\n", a)

	type (
		long int64
		char byte
	)
	var b long
	var c char
	fmt.Printf("  b type is %T\n", b)
	fmt.Printf("  c type is %T\n", c)
}
