package main

import abc "fmt"	//给导入的包名起别名
import _ "time"		//忽略此包，用处：调用包里的init函数


var a string

func main() {

	var a int

	//不同作用域，允许定义同名变量。使用 就近原则。

	{
		var a float32
		abc.Printf("(一)、块儿内部：[%T]\n",a)	//会用 块内 局部变量
	}

	abc.Printf("(二)、块儿外部：[%T]\n",a)	//会用 块外 局部变量

	test1()

}
func test1(){
	abc.Printf("(三)、test()：[%T]\n",a)	//会用 全局变量
}

