package main

import (
	"math"
	"fmt"
)
type MyFloat float64
/*
 * 方法的用法  以下全是猜想，不知正确与否。
 * Go没有类。但是，您可以在类型上定义方法。方法是带有特殊接收者参数的函数。接收方出现在func关键字和方法名之间的参数列表中。在这个例子中，Abs方法有一个名为v的类型顶点的接收器。
 */
type Ve struct {
	X, Y float64
}

/*
 * 可以在类型上定义方法。方法是带有特殊接收者参数的函数。接收方出现在func关键字和方法名之间的参数列表中。在这个例子中，Abs方法有一个名为v的类型顶点的接收器。
 * func (receiver 接收器)  方法名() 返回值 { }
 */
func (v Ve) Abs1() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// func 方法名(入参) 返回值 { }
func Abs2(v Ve) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

/*
 * 也可以在非结构类型上声明方法。 一个带有Abs3方法的数字类型MyFloat。 只能使用接收器声明一个方法，该接收器的类型与方法在同一个包中定义。
 * 您不能使用接收器声明一个方法，该接收器的类型在另一个包中定义（包括内置类型，如int）。
 */
func (f MyFloat) Abs3() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func main() {
	v := Ve{3, 4}		//定义个入参 接收器
	fmt.Println(v.Abs1())	//通过 接收器.方法，调用方法
	fmt.Printf("--------------------1----------------------\n")
	fmt.Println(Abs2(v))	//通过方法(入参)，调用方法
}
