package main
import "fmt"

/*
 *  结构的用法
 */
type Vertex struct {
	X int
	Y int
}

var (
	v1 = Vertex{1, 2}  // has type Vertex
	v2 = Vertex{X: 1}  // 暗含 Y:0
	v3 = Vertex{}      // X:0 , Y:0
	p  = &Vertex{1, 2} // has type *Vertex
)

func main() {

	fmt.Println("第 1 行：",v1, v2, v3, p)

	v1.X = 4				//设置结构变量的值
	fmt.Println("第 2 行：",v1.X)	//读取结构变量的值

	p := &v1		//p指向 v1的内存地址
	p.X = 1e9		//为p的X变量 重新赋值
	fmt.Println("第 3 行：",v1)	//看下v1的新值
}



