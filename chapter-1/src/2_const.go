package main
import (
	"fmt"
)

/*
 *  常量的用法
 */
func main() {
	const a int = 10
	//a=20 			//常量不允许修改。会报编译错误：cannot assign to a

	const b = 20.345  // 不写类型，也能自动判断出类型，注意：常量声明赋值 无需用:= ，只用等于号即可。

	const (
		c=2
		d=3.5		//常量 d 没有使用，没报错
	)

	//fmt.Printf("  a=%d,\n  b=%.3f,\n  c=%d\n\n", a,b,c)
	//
	//fmt.Printf("  b type is %T\n", b)

	var e int = 1
	var f float64 = 0.8
	g := float64(e) - f
	fmt.Print(g,"\n")
	g++;
	fmt.Print(g,"\n")
	fmt.Printf("g=%f", g)
}
